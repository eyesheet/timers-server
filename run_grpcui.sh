#!/bin/bash

export $(grep -v '^#' .env | xargs)
grpcui -import-path proto/ -proto proto/eyesheet.proto -plaintext "$TIMERS_SERVER_ADDR"
