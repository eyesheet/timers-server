set dotenv-load

run:
  cargo run

build-container:
  nix build .?submodules=1#containerImage

grpcui:
  grpcui -import-path proto/ -proto proto/eyesheet.proto -plaintext "$TIMERS_SERVER_ADDR"
