pub mod eyesheet {
    tonic::include_proto!("eyesheet");
}

use std::env;
use std::sync::Arc;

use eyesheet::timer_event::EventType;
use eyesheet::timers_server::{Timers, TimersServer};
use eyesheet::{GetTimerRequest, RemoveTimerRequest, TimerResponse};
use futures::{StreamExt, TryFutureExt, TryStreamExt};
use tokio::sync::mpsc;
use tokio_stream::wrappers::ReceiverStream;
use tokio_util::sync::PollSender;

use tonic::{transport::Server, Request, Response, Status};

mod auth;
mod data;
mod error;
mod events;

#[derive(Debug)]
struct TimerService {
    redis_client: Arc<redis::Client>,
}

async fn require_timer_control(
    redis_client: &redis::Client,
    user: &(impl auth::User + ?Sized),
    timer_id: u64,
) -> Result<(), tonic::Status> {
    if !data::user_controls_timer(
        &mut redis_client
            .get_async_connection()
            .await
            .map_err(error::redis_error_to_tonic_status)?,
        user,
        timer_id,
    )
    .await?
    {
        return Err(tonic::Status::permission_denied(
            "User was not allowed to access this timer",
        ));
    };
    Ok(())
}

#[tonic::async_trait]
impl Timers for TimerService {
    async fn get_timer(
        &self,
        request: Request<GetTimerRequest>,
    ) -> Result<Response<TimerResponse>, Status> {
        let mut conn = self
            .redis_client
            .get_async_connection()
            .await
            .map_err(|e| Status::unknown(e.to_string()))?;
        let user: &auth::BoxUser = request.extensions().get().unwrap();
        let message = request.get_ref();
        let id = message.id;
        require_timer_control(&self.redis_client, user, id).await?;
        let timer = data::get_countdown_by_id(&mut conn, id)
            .await
            .map_err(|e| Status::unknown(e.to_string()))?;
        Ok(Response::new(TimerResponse {
            id,
            timer: Some(timer),
        }))
    }

    type GetTimersStream = ReceiverStream<Result<TimerResponse, Status>>;

    async fn get_timers(
        &self,
        request: Request<()>,
    ) -> Result<Response<Self::GetTimersStream>, tonic::Status> {
        let mut conn = self
            .redis_client
            .get_async_connection()
            .await
            .map_err(|e| Status::unknown(e.to_string()))?;
        let (tx, rx) = mpsc::channel(4);

        tokio::spawn(async move {
            let user: &auth::BoxUser = request.extensions().get().unwrap();
            for id in data::get_timer_ids_for_user(&mut conn, user.as_ref())
                .await
                .unwrap()
            {
                let timer = data::get_countdown_by_id(&mut conn, id).await.ok();
                tx.send(Ok(TimerResponse { id, timer })).await?;
            }
            Ok::<_, mpsc::error::SendError<_>>(())
        });

        Ok(Response::new(ReceiverStream::new(rx)))
    }
    async fn remove_timer(
        &self,
        request: tonic::Request<RemoveTimerRequest>,
    ) -> Result<tonic::Response<()>, tonic::Status> {
        let mut conn = self
            .redis_client
            .get_async_connection()
            .await
            .map_err(|e| Status::unknown(e.to_string()))?;

        let message = request.get_ref();
        let user = auth::get_user_from_extensions(request.extensions())?;
        let id = message.id;
        require_timer_control(&self.redis_client, user, id).await?;
        if id == 0 {
            return Err(Status::out_of_range("The supplied id was 0"));
        }
        data::remove_countdown(&mut conn, id)
            .await
            .map_err(|e| Status::not_found(e.to_string()))?;
        events::notify_users(
            self.redis_client.clone(),
            EventType::Deleted,
            events::UserNotificationScope::List(
                [&user]
                    .into_iter()
                    .map(|u| auth::user_from_id(u.get_user_id()))
                    .collect(),
            ),
            id,
        )
        .await
        .map_err(|e| Status::not_found(e.to_string()))?;
        Ok(Response::new(()))
    }

    async fn add_timer(
        &self,
        mut request: Request<eyesheet::Timer>,
    ) -> Result<Response<TimerResponse>, Status> {
        let user: auth::BoxUser = request.extensions_mut().remove().unwrap();
        let message = request.get_ref();
        let mut conn = self
            .redis_client
            .get_async_connection()
            .await
            .map_err(|e| Status::unknown(e.to_string()))?;
        let id = data::add_countdown(&mut conn, message)
            .await
            .map_err(|e| Status::internal(e.to_string()))?;
        events::notify_users(
            self.redis_client.clone(),
            EventType::Created,
            events::UserNotificationScope::List(
                [&user]
                    .into_iter()
                    .map(|u| auth::user_from_id(u.get_user_id()))
                    .collect(),
            ),
            id,
        )
        .await
        .map_err(|e| Status::not_found(e.to_string()))?;
        data::assign_timer_to_user(&mut conn, user.as_ref(), id).await?;
        Ok(Response::new(TimerResponse {
            id,
            timer: Some(message.clone()),
        }))
    }

    type FollowTimerEventsStream = ReceiverStream<Result<eyesheet::TimerEvent, tonic::Status>>;

    async fn follow_timer_events(
        &self,
        mut request: tonic::Request<()>,
    ) -> Result<tonic::Response<Self::FollowTimerEventsStream>, tonic::Status> {
        let client = self.redis_client.clone();
        let (tx, rx) = mpsc::channel(4);

        let ps = PollSender::new(tx);

        let user: auth::BoxUser = request.extensions_mut().remove().unwrap();
        let future = events::follow_user_timer_events(client.clone(), user)
            .try_flatten_stream()
            .map_err(|err| tonic::Status::internal(err.to_string()))
            .map(Ok)
            .forward(ps);

        tokio::spawn(future);
        Ok(Response::new(ReceiverStream::new(rx)))
    }

    async fn change_timer_duration(
        &self,
        mut request: tonic::Request<eyesheet::TimerDurationChange>,
    ) -> Result<tonic::Response<()>, tonic::Status> {
        let client = self.redis_client.clone();
        let user = auth::take_user_from_extensions(request.extensions_mut())?;
        let message = request.get_ref();
        require_timer_control(&client, &user, message.id).await?;
        data::change_countdown_duration(
            &mut client
                .get_async_connection()
                .await
                .map_err(error::redis_error_to_tonic_status)?,
            message.id,
            message.duration_change,
        )
        .await?;
        events::notify_users(
            self.redis_client.clone(),
            EventType::TimeUpdated,
            events::UserNotificationScope::List(vec![user]),
            message.id,
        )
        .await
        .map_err(|e| Status::not_found(e.to_string()))?;
        Ok(Response::new(()))
    }

    async fn pause_timer(
        &self,
        mut request: tonic::Request<eyesheet::TimerPauseRequest>,
    ) -> Result<tonic::Response<()>, tonic::Status> {
        let client = self.redis_client.clone();
        let user = auth::take_user_from_extensions(request.extensions_mut())?;
        let message = request.get_ref();
        require_timer_control(&client, &user, message.timer_id).await?;
        data::pause_timer(
            &mut client
                .get_async_connection()
                .await
                .map_err(error::redis_error_to_tonic_status)?,
            message.timer_id,
            message.remaining_time,
        )
        .await?;
        events::notify_users(
            self.redis_client.clone(),
            EventType::Paused,
            events::UserNotificationScope::List(vec![user]),
            message.timer_id,
        )
        .await
        .map_err(|e| Status::unknown(e.to_string()))?;
        Ok(Response::new(()))
    }

    async fn resume_timer(
        &self,
        mut request: tonic::Request<eyesheet::TimerResumeRequest>,
    ) -> Result<tonic::Response<()>, tonic::Status> {
        let client = self.redis_client.clone();
        let user = auth::take_user_from_extensions(request.extensions_mut())?;
        let message = request.get_ref();
        require_timer_control(&client, &user, message.timer_id).await?;
        data::resume_timer(
            &mut client
                .get_async_connection()
                .await
                .map_err(error::redis_error_to_tonic_status)?,
            message.timer_id,
            message.end_time,
        )
        .await?;
        events::notify_users(
            self.redis_client.clone(),
            EventType::Resumed,
            events::UserNotificationScope::List(vec![user]),
            message.timer_id,
        )
        .await
        .map_err(|e| Status::unknown(e.to_string()))?;
        Ok(Response::new(()))
    }
}

fn interceptor(request: Request<()>) -> Result<Request<()>, tonic::Status> {
    Ok(request).and_then(auth::auth_interceptor)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv()?;
    let addr = env::var("TIMERS_SERVER_ADDR")?.parse()?;

    let redis_client = Arc::new(redis::Client::open(env::var("TIMERS_REDIS_ADDR")?)?);
    let timers = TimerService { redis_client };
    let svc = TimersServer::with_interceptor(timers, interceptor);

    Server::builder().add_service(svc).serve(addr).await?;
    Ok(())
}
