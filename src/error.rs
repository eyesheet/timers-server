use thiserror::Error;

#[derive(Error, Debug)]
pub enum DataError {
    #[error("Error whilst accessing Redis: {self}")]
    RedisError(#[from] redis::RedisError),
    #[error("{self}")]
    Status(#[from] tonic::Status),
    #[error("{self}")]
    JsonError(#[from] serde_json::Error),
}

pub type DataResult<T> = Result<T, DataError>;

pub fn redis_error_to_tonic_status(err: redis::RedisError) -> tonic::Status {
    use tonic::Status;
    let f = match err.kind() {
        redis::ErrorKind::ResponseError => Status::invalid_argument,
        redis::ErrorKind::AuthenticationFailed => Status::unauthenticated,
        redis::ErrorKind::TypeError => Status::failed_precondition,
        redis::ErrorKind::ExecAbortError => Status::aborted,
        redis::ErrorKind::BusyLoadingError => Status::resource_exhausted,
        redis::ErrorKind::NoScriptError => Status::not_found,
        redis::ErrorKind::InvalidClientConfig => Status::internal,
        redis::ErrorKind::Moved => todo!(),
        redis::ErrorKind::Ask => todo!(),
        redis::ErrorKind::TryAgain => todo!(),
        redis::ErrorKind::ClusterDown => todo!(),
        redis::ErrorKind::CrossSlot => todo!(),
        redis::ErrorKind::MasterDown => Status::internal,
        redis::ErrorKind::IoError => Status::unknown,
        redis::ErrorKind::ClientError => todo!(),
        redis::ErrorKind::ExtensionError => Status::internal,
        redis::ErrorKind::ReadOnly => Status::permission_denied,
        _ => todo!(),
    };
    f(err.to_string())
}

impl From<DataError> for tonic::Status {
    fn from(err: DataError) -> Self {
        match err {
            DataError::RedisError(err) => redis_error_to_tonic_status(err),
            DataError::Status(s) => s,
            DataError::JsonError(err) => match err.classify() {
                serde_json::error::Category::Io => {
                    tonic::Status::internal("Error whilst reading or writing JSON")
                }
                serde_json::error::Category::Syntax => {
                    tonic::Status::invalid_argument("Syntax error whilst reading or writing JSON")
                }
                serde_json::error::Category::Data => tonic::Status::invalid_argument(
                    "Error input data that is semantically incorrect",
                ),
                serde_json::error::Category::Eof => tonic::Status::invalid_argument(
                    "Unexpcted end of file whilst reading or writing JSON",
                ),
            },
        }
    }
}
