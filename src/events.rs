use std::sync::Arc;

use futures::{Stream, StreamExt};
use redis::{AsyncCommands, RedisError};
use serde::{Deserialize, Serialize};

use crate::eyesheet::timer_event::EventType;
use crate::eyesheet::TimerEvent;
use crate::{auth, data, error};

#[derive(Serialize, Deserialize)]
pub enum UserNotificationScope<U: auth::User> {
    #[serde(
        serialize_with = "serialize_user_list",
        deserialize_with = "deserialize_user_list",
        bound(deserialize = "U: From<auth::BoxUser>")
    )]
    List(Vec<U>),
    All,
}

impl<U: auth::User> UserNotificationScope<U> {
    pub fn map_user<To: auth::User>(self, f: impl (FnMut(U) -> To)) -> UserNotificationScope<To> {
        match self {
            UserNotificationScope::List(list) => {
                UserNotificationScope::<To>::List(list.into_iter().map(f).collect())
            }
            UserNotificationScope::All => UserNotificationScope::<To>::All,
        }
    }
}

fn serialize_user_list<S, U: auth::User>(list: &Vec<U>, seralizer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    list.iter()
        .map(U::get_user_id)
        .map(|id| id.to_string())
        .collect::<Vec<_>>()
        .serialize(seralizer)
}

fn deserialize_user_list<'de, D, U>(deserializer: D) -> Result<Vec<U>, D::Error>
where
    D: serde::Deserializer<'de>,
    U: auth::User + From<auth::BoxUser>,
{
    Vec::<String>::deserialize(deserializer).map(|res| {
        res.into_iter()
            .map(|s| uuid::Uuid::parse_str(&s).unwrap()) // TODO fix error handling
            .map(|id| auth::user_from_id(id))
            .map(|u| u.into())
            .collect::<Vec<U>>()
    })
}

async fn pubsub_stream(
    client: Arc<redis::Client>,
    channels: &[&str],
) -> Result<impl Stream<Item = redis::Msg>, RedisError> {
    let mut pubsub = client.get_async_connection().await?.into_pubsub();
    for channel in channels {
        pubsub.subscribe(channel).await?;
    }
    Ok(pubsub.into_on_message())
}

fn parse_redis_msg_json(msg: redis::Msg) -> error::DataResult<serde_json::Value> {
    Ok(serde_json::from_str(msg.get_payload::<String>()?.as_ref())?)
}

pub async fn follow_timer_events<'a>(
    client: Arc<redis::Client>,
) -> error::DataResult<impl Stream<Item = error::DataResult<TimerEvent>> + 'a> {
    let stream = pubsub_stream(client.clone(), &["timer_events"]).await?;

    Ok(stream.then(move |msg| {
        let client = client.clone();
        async move {
            let mut conn = client.get_async_connection().await?;
            let payload = parse_redis_msg_json(msg)?;
            let err = || tonic::Status::failed_precondition("Wrong data passed on event pubsub");
            let id = payload["id"].as_u64().ok_or_else(err)?;
            let event_type = payload["type"].as_i64().ok_or_else(err)?;
            Ok(TimerEvent {
                timer_id: id,
                event_type: event_type.try_into().unwrap(),
                timer: data::get_countdown_by_id(&mut conn, id).await.ok(),
            })
        }
    }))
}

pub async fn follow_user_timer_events<'a, U: auth::User + Send + Sync + 'static>(
    client: Arc<redis::Client>,
    user: U,
) -> error::DataResult<impl Stream<Item = error::DataResult<TimerEvent>> + 'a> {
    let user = Arc::new(user);
    let stream = pubsub_stream(client.clone(), &["user_timer_events"]).await?;
    Ok(stream
        .then(move |msg| {
            let client = client.clone();
            let user = user.clone();
            async move {
                let mut conn = client.get_async_connection().await?;
                let mut payload = parse_redis_msg_json(msg)?;
                let err =
                    || tonic::Status::failed_precondition("Wrong data passed on event pubsub");
                let id = payload["id"].as_u64().ok_or_else(err)?;
                let event_type = payload["type"].as_i64().ok_or_else(err)?;
                let scope: UserNotificationScope<auth::BoxUser> =
                    serde_json::from_value(payload["scope"].take())?;
                use UserNotificationScope::*;
                let in_scope = match scope {
                    List(l) => l
                        .iter()
                        .find(|el| el.get_user_id() == user.get_user_id())
                        .is_some(),
                    All => true,
                };
                if in_scope {
                    Ok(Some(TimerEvent {
                        timer_id: id,
                        event_type: event_type.try_into().unwrap(),
                        timer: data::get_countdown_by_id(&mut conn, id).await.ok(),
                    }))
                } else {
                    Ok(None)
                }
            }
        })
        .filter_map(|v| async move { v.transpose() }))
}

pub async fn notify(
    client: Arc<redis::Client>,
    event_type: EventType,
    id: u64,
) -> error::DataResult<()> {
    let mut conn = client.get_async_connection().await?;
    Ok(conn
        .publish(
            "timer_events",
            serde_json::json!({
                "id": id,
                "type": event_type as i32,
            })
            .to_string(),
        )
        .await?)
}

pub async fn notify_users<U: auth::User>(
    client: Arc<redis::Client>,
    event_type: EventType,
    scope: UserNotificationScope<U>,
    id: u64,
) -> error::DataResult<()> {
    let mut conn = client.get_async_connection().await?;
    Ok(conn
        .publish(
            "user_timer_events",
            serde_json::json!({
                "id": id,
                "type": event_type as i32,
                "scope": scope,
            })
            .to_string(),
        )
        .await?)
}
