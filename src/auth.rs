use std::io::Read;

use tonic::Request;
use uuid::Uuid;

pub trait User {
    fn get_user_id(&self) -> Uuid;
    fn boxed(self) -> BoxUser
    where
        Self: Sized + Sync + Send + 'static,
    {
        Box::new(self)
    }
}

#[derive(Debug, Clone)]
struct UuidUser {
    user_id: Uuid,
}

impl User for UuidUser {
    fn get_user_id(&self) -> Uuid {
        self.user_id
    }
}

pub fn user_from_id(user_id: Uuid) -> BoxUser {
    UuidUser { user_id }.boxed()
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct Claims {
    sub: String,
}

fn load_decoding_key() -> jsonwebtoken::DecodingKey {
    let mut f = std::fs::File::open(std::env::var("JWT_PUB_KEY").unwrap()).unwrap();
    let mut v = vec![];
    f.read_to_end(&mut v).unwrap();
    jsonwebtoken::DecodingKey::from_rsa_pem(&v).unwrap()
}

fn user_from_metadata(metadata: &tonic::metadata::MetadataMap) -> Result<BoxUser, tonic::Status> {
    let authorization = metadata
        .get("authorization")
        .and_then(|v| v.to_str().ok())
        .ok_or(tonic::Status::unauthenticated(
            "No authorization header provided",
        ));
    let (scheme, val) = authorization.and_then(|v| {
        v.split_once(' ').ok_or(tonic::Status::unauthenticated(
            "Invalid Authorization header",
        ))
    })?;
    match scheme {
        "Bearer" => {
            let key = load_decoding_key();
            let token = jsonwebtoken::decode::<Claims>(
                val,
                &key,
                &jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::RS384),
            )
            .map_err(|_| tonic::Status::unauthenticated("Invalid JWT"))?;
            let user_id = Uuid::parse_str(&token.claims.sub).unwrap();
            Ok(UuidUser { user_id }.boxed())
        }
        _ => Err(tonic::Status::unauthenticated(
            "Invalid Authorization scheme",
        )),
    }
}

pub type BoxUser = Box<dyn User + Send + Sync + 'static>;

impl User for BoxUser {
    fn get_user_id(&self) -> Uuid {
        self.as_ref().get_user_id()
    }

    fn boxed(self) -> BoxUser {
        self
    }
}

pub fn auth_interceptor(mut request: Request<()>) -> Result<Request<()>, tonic::Status> {
    let metadata = request.metadata();
    let user = user_from_metadata(metadata);
    //dbg!(user.as_ref().map(|u| u.get_user_id()));
    request.extensions_mut().insert::<BoxUser>(user?);
    Ok(request)
}

pub fn take_user_from_extensions(
    extensions: &mut tonic::Extensions,
) -> Result<BoxUser, tonic::Status> {
    extensions.remove().ok_or(tonic::Status::unauthenticated(
        "User authentication not supplied.",
    ))
}

pub fn get_user_from_extensions(extensions: &tonic::Extensions) -> Result<&BoxUser, tonic::Status> {
    extensions.get().ok_or(tonic::Status::unauthenticated(
        "User authentication not supplied.",
    ))
}

pub fn mut_user_from_extensions(
    extensions: &mut tonic::Extensions,
) -> Result<&mut BoxUser, tonic::Status> {
    extensions.get_mut().ok_or(tonic::Status::unauthenticated(
        "User authentication not supplied.",
    ))
}
