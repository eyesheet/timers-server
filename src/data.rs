use redis::{AsyncCommands, RedisResult};

use crate::{
    auth,
    error::{self, DataResult},
    eyesheet::Timer,
};

fn namespace_key<K: ToString>(namespace: &str, key: K) -> String {
    format!("{}:{}", namespace, key.to_string())
}

fn timer_paused(timer: &crate::eyesheet::timer::State) -> bool {
    use crate::eyesheet::timer::State;
    match timer {
        State::Running(_) => false,
        State::Paused(_) => true,
    }
}

pub async fn add_countdown<Conn: AsyncCommands>(conn: &mut Conn, timer: &Timer) -> DataResult<u64> {
    let id: u64 = conn.incr("countdown:id", 1).await?;
    conn.sadd::<_, _, ()>("countdown_ids", id).await?;
    let timer_state = timer
        .state
        .as_ref()
        .ok_or(tonic::Status::failed_precondition(
            "Timer was strucutred as expected",
        ))?;
    use crate::eyesheet::timer::State;
    if timer.total_time == 0
        || match timer_state {
            State::Running(state) => state.end_time == 0,
            State::Paused(state) => state.remaining_time == 0,
        }
    {
        return Err(error::DataError::Status(tonic::Status::out_of_range(
            "The time supplied was a zero value",
        )));
    }
    let key = namespace_key("countdown", id);
    conn.hset_multiple::<_, _, _, ()>(
        &key,
        &[
            match timer_state {
                State::Running(state) => ("end", state.end_time),
                State::Paused(state) => ("remaining", state.remaining_time),
            },
            ("total", timer.total_time),
        ],
    )
    .await?;
    conn.hset(key, "paused", timer_paused(timer_state)).await?;
    Ok(id)
}

pub async fn remove_countdown<Conn: AsyncCommands>(conn: &mut Conn, id: u64) -> RedisResult<()> {
    conn.srem::<_, _, ()>("countdown_ids", id).await?;
    conn.del::<_, ()>(namespace_key("countdown", id)).await?;
    Ok(())
}

pub async fn get_countdown_ids<Conn: AsyncCommands>(conn: &mut Conn) -> RedisResult<Vec<u64>> {
    conn.smembers("countdown_ids").await
}

fn key_for_countdown(id: u64) -> String {
    namespace_key("countdown", id)
}

fn key_for_user_countdown_list(user_id: uuid::Uuid) -> String {
    namespace_key("user_countdowns", user_id)
}

pub async fn get_timer_ids_for_user<Conn: AsyncCommands>(
    conn: &mut Conn,
    user: &(impl auth::User + ?Sized),
) -> RedisResult<Vec<u64>> {
    conn.smembers(key_for_user_countdown_list(user.get_user_id()))
        .await
}

pub async fn user_controls_timer<Conn: AsyncCommands>(
    conn: &mut Conn,
    user: &(impl auth::User + ?Sized),
    timer_id: u64,
) -> error::DataResult<bool> {
    Ok(conn
        .sismember(key_for_user_countdown_list(user.get_user_id()), timer_id)
        .await?)
}

pub async fn assign_timer_to_user<Conn: AsyncCommands>(
    conn: &mut Conn,
    user: &(impl auth::User + ?Sized),
    timer_id: u64,
) -> error::DataResult<()> {
    conn.sadd(key_for_user_countdown_list(user.get_user_id()), timer_id)
        .await?;
    Ok(())
}

pub async fn countdown_with_id_exists<Conn: AsyncCommands>(
    conn: &mut Conn,
    id: u64,
) -> error::DataResult<bool> {
    let key = key_for_countdown(id);
    Ok(conn.hexists(key.clone(), "paused").await?)
}

pub async fn get_countdown_by_id<Conn: AsyncCommands>(
    conn: &mut Conn,
    id: u64,
) -> Result<Timer, redis::RedisError> {
    let key = key_for_countdown(id);
    let paused = conn.hget(&key, "paused").await?;
    let total_time = conn.hget(&key, "total").await?;
    use crate::eyesheet::timer;
    let state = Some(if paused {
        let remaining = conn.hget(key, "remaining").await?;
        timer::State::Paused(timer::Paused {
            remaining_time: remaining,
        })
    } else {
        let end_time = conn.hget(key, "end").await?;
        timer::State::Running(timer::Running { end_time })
    });
    Ok(Timer { total_time, state })
}

pub async fn change_countdown_duration<Conn: AsyncCommands>(
    conn: &mut Conn,
    id: u64,
    change_amount: i64,
) -> error::DataResult<()> {
    let key = key_for_countdown(id);
    let paused = conn.hget(&key, "paused").await?;
    if paused {
        conn.hincr(key, "remaining", change_amount).await?;
    } else {
        conn.hincr(key, "end", change_amount).await?;
    }
    Ok(())
}

pub async fn pause_timer<Conn: AsyncCommands>(
    conn: &mut Conn,
    id: u64,
    remaining_time: Option<i64>,
) -> error::DataResult<()> {
    let key = key_for_countdown(id);
    let paused = conn.hget(&key, "paused").await?;
    let remaining_time = match remaining_time {
        Some(v) => v,
        None => {
            if paused {
                conn.hget(&key, "remaining").await?
            } else {
                let end: i64 = conn.hget(&key, "end").await?;
                end - (time::OffsetDateTime::now_utc().unix_timestamp_nanos() / 1_000_000) as i64
            }
        }
    };
    if !paused {
        conn.hdel(&key, "end").await?;
    }
    conn.hset(&key, "remaining", remaining_time).await?;
    conn.hset(&key, "paused", true).await?;
    Ok(())
}

pub async fn resume_timer<Conn: AsyncCommands>(
    conn: &mut Conn,
    id: u64,
    end_time: Option<i64>,
) -> error::DataResult<()> {
    let key = key_for_countdown(id);
    let paused = conn.hget(&key, "paused").await?;
    let end_time = match end_time {
        Some(v) => v,
        None => {
            if paused {
                let remaining: i64 = conn.hget(&key, "remaining").await?;
                remaining
                    + (time::OffsetDateTime::now_utc().unix_timestamp_nanos() / 1_000_000) as i64
            } else {
                conn.hget(&key, "end").await?
            }
        }
    };
    if paused {
        conn.hdel(&key, "remaining").await?;
    }
    conn.hset(&key, "end", end_time).await?;
    conn.hset(&key, "paused", false).await?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn namespace_key_test() {
        assert_eq!(namespace_key("testnamespace", 33), "testnamespace:33");
        assert_eq!(
            namespace_key("testnamespace_underscores", 33),
            "testnamespace_underscores:33"
        );
    }

    #[test]
    fn countdown_key_test() {
        assert_eq!(key_for_countdown(33), "countdown:33");
    }

    #[test]
    fn user_countdown_list_key_test() {
        assert_eq!(
            key_for_user_countdown_list(uuid::uuid!("00000000-0000-0000-0000-ffff00000001")),
            "user_countdowns:00000000-0000-0000-0000-ffff00000001"
        );
    }
}
