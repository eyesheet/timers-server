{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, flake-utils, naersk, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = (import nixpkgs) {
          inherit system;
        };

        nativeBuildInputs = with pkgs; [ pkg-config cmake ];
        buildInputs = with pkgs; [ openssl protobuf ];

        naersk' = pkgs.callPackage naersk {};

      in rec {
        # For `nix build` & `nix run`:
        defaultPackage = naersk'.buildPackage {
          src = ./.;
          inherit nativeBuildInputs;
          inherit buildInputs;
        };

        # For `nix develop`:
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [ rustc cargo ] ++ nativeBuildInputs;
          inherit buildInputs;
        };

        packages.containerImage = pkgs.dockerTools.buildLayeredImage {
          name = "user-service";
          config = {
            Entrypoint = [ "${defaultPackage}/bin/user-service" ];
            Env = [
              "USERS_SERVICE_ADDR=[::]:43400"
            ];
            ExposedPorts = {
              "43400" = {};
            };
          };
        };
      }
    );
}
